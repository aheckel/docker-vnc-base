FROM debian:bullseye-slim


ARG DEBIAN_FRONTEND=noninteractive

ENV USER=vnc
ENV HOME=/home/${USER}

ENV VNC_DEPTH=24
ENV VNC_DISPLAY=:1
ENV VNC_GEOMETRY=1600x1024
ENV VNC_PASSWORD=secret


RUN apt-get update && \
    apt-get install --no-install-recommends --yes eterm fluxbox tightvncserver x11-xserver-utils xfonts-base && \
    rm --force --recursive /var/lib/apt/lists/*

RUN useradd --create-home --home-dir ${HOME} --shell /bin/bash ${USER} && \
    mkdir ${HOME}/.vnc

COPY xstartup ${HOME}/.vnc/xstartup
COPY docker-entrypoint.sh ${HOME}/docker-entrypoint.sh

RUN chown ${USER}:${USER} --recursive ${HOME} && \
    chmod +x ${HOME}/docker-entrypoint.sh ${HOME}/.vnc/xstartup


USER ${USER}

WORKDIR ${HOME}

EXPOSE 5901

CMD ["/home/vnc/docker-entrypoint.sh"]
