#!/bin/bash

echo ${VNC_PASSWORD} | vncpasswd -f > ${HOME}/.vnc/passwd
chmod 600 ${HOME}/.vnc/passwd

vncserver ${VNC_DISPLAY} -geometry 1600x1024 -depth 24

sleep infinity

